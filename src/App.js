import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import NavBar from './components/NavBar';
import Footer from './components/pages/Footer/Footer';
import Home from './components/pages/HomePage/Home';
import Services from './components/pages/Services/Services';

function App() {
  return (
    <Router className="App">
      <NavBar />
      <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/services"  component={Services}/>
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
